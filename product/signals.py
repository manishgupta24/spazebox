from typing import Union
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.utils.text import slugify
from rest_framework import serializers

from product.models import (Brand, Category, Product, SubCategory)


@receiver(pre_save, sender=Brand)
@receiver(pre_save, sender=Category)
@receiver(pre_save, sender=Product)
@receiver(pre_save, sender=SubCategory)
def set_slug_value(sender,
                   instance: Union[Brand, Category, Product, SubCategory],
                   raw: bool, **kwargs):
    if raw:
        return

    if not instance.pk:
        slug = slugify(instance.name)
        if sender.objects.filter(slug=slug).exists():
            raise serializers.ValidationError({
                "name": ["Product with same name and slug exists"]
            })
        instance.slug = slug
