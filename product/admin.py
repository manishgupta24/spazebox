from django.contrib import admin

from product.models import (Brand, Category, Inventory, MeasurementUnit,
                            Product, ProductVariant, SubCategory)

admin.site.register(
    MeasurementUnit,
    list_display=('id', 'name', 'code'),
    search_fields=('id', 'name', 'code'))

admin.site.register(
    Brand, list_display=('id', 'name'), search_fields=('id', 'name'))

admin.site.register(
    Category, list_display=('id', 'name'), search_fields=('id', 'name'))

admin.site.register(
    SubCategory,
    list_display=('id', 'name', 'category'),
    search_fields=('id', 'name', 'category__name'),
    raw_id_fields=('category', ))

admin.site.register(
    Product,
    list_display=('id', 'name', 'code', 'brand', 'category', 'subcategory'),
    search_fields=('id', 'name', 'category__name', 'subcategory__name',
                   'brand__name', 'code'),
    raw_id_fields=('category', 'subcategory', 'brand', 'created_by',
                   'modified_by'))

admin.site.register(
    ProductVariant,
    list_display=('id', 'product', 'code', 'box_quantity',
                  'measurement_quantity', 'measurement_unit'),
    search_fields=('id', 'product__name', 'code'),
    raw_id_fields=('product', 'measurement_unit', 'created_by', 'modified_by'))

admin.site.register(
    Inventory,
    list_display=('id', 'product_variant', 'available_stock', 'sale_price'),
    search_fields=('id', 'product_variant__product__name',
                   'product_variant__code'),
    raw_id_fields=('product_variant', 'created_by', 'modified_by'))
