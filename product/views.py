from rest_framework import mixins, response, status, views, viewsets
from rest_framework.permissions import AllowAny

from product.models import Brand, Category, Product
from product.serializers import BrandSerializer, CategoryListSerializer, CategorySerializer, ProductSerializer


class BrandView(mixins.ListModelMixin, viewsets.GenericViewSet):

    queryset = Brand.objects.all()
    serializer_class = BrandSerializer
    permission_classes = (AllowAny, )


class CategoryView(mixins.ListModelMixin, viewsets.GenericViewSet):

    queryset = Category.objects.prefetch_related("subcategory").all()
    serializer_class = CategoryListSerializer
    permission_classes = (AllowAny, )


class ProductView(mixins.ListModelMixin, mixins.RetrieveModelMixin,
                  viewsets.GenericViewSet):

    queryset = Product.objects.prefetch_related(
        "productvariant", "productvariant__inventory").all()
    serializer_class = ProductSerializer
    permission_classes = (AllowAny, )
