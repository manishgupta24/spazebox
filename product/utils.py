def s3_storage_product_image(instance, filename: str):
    # Can't define type hint for instance arg to prevent circular dependency.
    return "product/images/{}/{}".format(str(instance.pk), filename)
