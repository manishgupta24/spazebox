from django.urls import include, path
from rest_framework import routers

from product.views import BrandView, CategoryView, ProductView


def get_router():
    router = routers.DefaultRouter()
    router.register('brand', BrandView)
    router.register('category', CategoryView)
    router.register('product', ProductView)
    return router


urlpatterns = [path('', include(get_router().urls))]

app_name = 'products'
