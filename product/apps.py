from django.apps import AppConfig


class ProductConfig(AppConfig):
    name = 'product'

    def ready(self, *args, **kwargs):
        super(ProductConfig, self).ready(*args, **kwargs)

        from product import signals
