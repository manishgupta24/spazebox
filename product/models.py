from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.utils.translation import ugettext_lazy as _
from djmoney.models.fields import MoneyField
from storages.backends.s3boto3 import S3Boto3Storage

from core.model_mixins import UserTimestampLogMixin
from product.utils import s3_storage_product_image


class MeasurementUnit(models.Model):
    """
    Measurement Model.
    To store different types of measurement units.
    e.g ML (Millilitre), L (Litre), G (Gram), KG (Kilogram) etc.
    """
    name = models.CharField(max_length=20)
    code = models.CharField(max_length=20)

    def __str__(self):
        return self.name


class Brand(models.Model):
    """
    Brand Model.
    To store different types of brand names.
    e.g Coca Cola, Parle etc.
    """
    name = models.CharField(max_length=20)
    slug = models.CharField(
        max_length=20,
        unique=True,
        blank=True,
        help_text=_(
            "The name in all lowercase, suitable for URL identification"))

    def __str__(self):
        return self.name


class Category(models.Model):
    """
    Category Model.
    To store different types of category names.
    e.g Beverages, Snacks etc.
    """
    name = models.CharField(max_length=20)
    slug = models.CharField(
        max_length=20,
        unique=True,
        blank=True,
        help_text=_(
            "The name in all lowercase, suitable for URL identification"))

    def __str__(self):
        return self.name


class SubCategory(models.Model):
    """
    SubCategory Model.
    To store different types of category names.
    e.g SoftDrinks, Cookies etc.
    """
    category = models.ForeignKey(
        "product.Category",
        related_name="subcategory",
        on_delete=models.DO_NOTHING)
    name = models.CharField(max_length=20)
    slug = models.CharField(
        max_length=20,
        unique=True,
        blank=True,
        help_text=_(
            "The name in all lowercase, suitable for URL identification"))

    def __str__(self):
        return self.name


class Product(UserTimestampLogMixin):
    """
    Product Model.
    To store the data related to products.
    """
    category = models.ForeignKey(
        "product.Category",
        related_name="product",
        on_delete=models.DO_NOTHING)
    subcategory = models.ForeignKey(
        "product.SubCategory",
        related_name="product",
        on_delete=models.DO_NOTHING)
    brand = models.ForeignKey(
        "product.Brand", related_name="product", on_delete=models.DO_NOTHING)
    name = models.CharField(max_length=80)
    slug = models.CharField(
        max_length=20,
        unique=True,
        blank=True,
        help_text=_(
            "The name in all lowercase, suitable for URL identification"))
    code = models.PositiveIntegerField(
        unique=True,
        validators=[MaxValueValidator(99999),
                    MinValueValidator(10000)],
        help_text=_("5 digit numerical code to uniquely identify a product"))
    description = models.TextField()
    manufacturer_description = models.TextField()

    def __str__(self):
        return self.name


class ProductVariant(UserTimestampLogMixin):
    """
    ProductVariant Model.
    To store different variants of a product based on measument_quantity/box_quantity.
    """
    product = models.ForeignKey(
        "product.Product",
        related_name="productvariant",
        on_delete=models.DO_NOTHING)
    measurement_unit = models.ForeignKey(
        "product.MeasurementUnit",
        related_name="productvariant",
        on_delete=models.DO_NOTHING)
    code = models.PositiveIntegerField(
        unique=True,
        validators=[MaxValueValidator(999999),
                    MinValueValidator(100000)],
        help_text=_(
            "6 digit numerical code to uniquely identify a product variant"))
    box_quantity = models.PositiveIntegerField(
        validators=[MaxValueValidator(50),
                    MinValueValidator(1)],
        help_text=_("Quantity of the product/combo in the box"))
    measurement_quantity = models.PositiveIntegerField(
        validators=[MinValueValidator(1)],
        help_text=_(
            "Measument quantity of a single item in product (Weight/Volume)"))
    image = models.FileField(
        storage=S3Boto3Storage(),
        help_text=_("Field to store the url of product image"),
        upload_to=s3_storage_product_image)
    is_combo = models.BooleanField(default=False)
    combo_items = models.ManyToManyField("self", related_name="combo")

    def __str__(self):
        return self.product.name + " -> " + self.code


class Inventory(UserTimestampLogMixin):
    """
    Inventory Model.
    To store the inventory and manage the pricing of a product variant.
    """
    product_variant = models.OneToOneField(
        "product.ProductVariant",
        related_name="inventory",
        on_delete=models.DO_NOTHING)
    available_stock = models.PositiveIntegerField(
        validators=[MinValueValidator(0)],
        help_text=_("Current stock in inventory for this variant"))
    sale_price = MoneyField(
        max_digits=8,
        decimal_places=2,
        help_text=_("Spazebox Selling price for the variant"))
    mrp_price = MoneyField(
        max_digits=8, decimal_places=2, help_text=_("MRP for the variant"))

    def __str__(self):
        return "{} -> {} ({}, {})".format(
            self.product_variant.product.name, self.product_variant.code,
            self.available_stock, self.sale_price)
