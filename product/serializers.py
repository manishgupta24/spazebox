from drf_writable_nested import WritableNestedModelSerializer
from rest_framework import serializers

from product.models import (Brand, Category, Inventory, MeasurementUnit,
                            Product, ProductVariant, SubCategory)


class MeasurementUnitSerializer(serializers.ModelSerializer):
    class Meta:
        model = MeasurementUnit
        exclude = []


class BrandSerializer(serializers.ModelSerializer):
    class Meta:
        model = Brand
        exclude = []


class SubCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = SubCategory
        exclude = []


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        exclude = []


class CategoryListSerializer(serializers.ModelSerializer):
    subcategory = SubCategorySerializer(many=True)

    class Meta:
        model = Category
        exclude = []


class InventorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Inventory
        exclude = []


class ProductVariantSerializer(WritableNestedModelSerializer):
    measurement_unit = MeasurementUnitSerializer(read_only=True)
    inventory = InventorySerializer()

    class Meta:
        model = ProductVariant
        exclude = []


class ProductSerializer(serializers.ModelSerializer):
    category = CategorySerializer(read_only=True)
    subcategory = SubCategorySerializer(read_only=True)
    brand = BrandSerializer(read_only=True)
    productvariant = ProductVariantSerializer(many=True)

    class Meta:
        model = Product
        exclude = []