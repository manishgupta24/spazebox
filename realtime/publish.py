import json
from typing import List

from paho.mqtt.client import Client as MQTTClient


class PublishMessage:
    def __init__(self, topics: List[str], payload: dict):
        self.topics = topics
        self.payload = payload

        self.client = MQTTClient(clean_session=False)
        self.client.username_pw_set("user", password="password")

        self.client.connect("localhost", port=1883)
        self.client.subscribe("test", qos=2)

        self._publish_message()

    def _publish_message(self):
        for topic in self.topics:
            self.client.publish(topic, payload=json.dumps(self.payload), qos=2)
