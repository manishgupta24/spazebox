from paho.mqtt.client import Client as MQTTClient


class Subscribe:
    def __init__(self):
        self.client = MQTTClient(clean_session=False)
        self.client.username_pw_set("user", password="password")

        self.client.on_message = self.on_message
        self.client.on_connect = self.on_connect
        self.client.on_publish = self.on_publish
        self.client.on_subscribe = self.on_subscribe

        self.client.connect("localhost", port=1883)
        # List of tuples for multiple topics
        self.client.subscribe([("test", 2)])
        self.client.loop_forever()

    def on_connect(self, mqttc, obj, flags, rc):
        print("rc: " + str(rc))

    def on_message(self, mqttc, obj, msg):
        print(msg.topic + " " + str(msg.qos) + " " + str(msg.payload))

    def on_publish(self, mqttc, obj, mid):
        print("mid: " + str(mid))

    def on_subscribe(self, mqttc, obj, mid, granted_qos):
        print("Subscribed: " + str(mid) + " " + str(granted_qos))


Subscribe()