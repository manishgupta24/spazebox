from django.core.validators import MinValueValidator
from django.db import models
from django.utils.translation import ugettext_lazy as _
from djmoney.models.fields import MoneyField
from phonenumber_field.modelfields import PhoneNumberField

from core.fields import INGstNumberField, INPanNumberField
from core.model_mixins import UserTimestampLogMixin


class Supplier(UserTimestampLogMixin):
    """
    Supplier Model.
    To store the supplier's/wholesaler's/manufacturer's details.
    """
    name = models.CharField(max_length=50)
    phone = PhoneNumberField()
    gst_number = INGstNumberField(null=True, blank=True, default=None)
    pan_number = INPanNumberField(null=True, blank=True, default=None)
    brands = models.ManyToManyField("product.Brand", related_name="supplier")

    def __str__(self):
        return self.name


class PurchaseOrder(UserTimestampLogMixin):
    """
    PurchaseOrder Model.
    To store/create purchase orders of products.
    """
    supplier = models.ForeignKey(
        "purchase.Supplier",
        related_name="purchaseorder",
        on_delete=models.DO_NOTHING)
    fulfillment_date = models.DateTimeField(
        help_text=_("Last date by which the purchase should be made.\
            After this date the purchase order will marked as invalid. "))

    def __str__(self):
        return self.supplier.name + " -> " + self.fulfillment_date


class PurchaseOrderItem(UserTimestampLogMixin):
    """
    PurchaseOrderItem Model.
    To store details of product ordered in a purchase order.
    """
    purchase_order = models.ForeignKey(
        "purchase.PurchaseOrder",
        related_name="purchase_order_item",
        on_delete=models.CASCADE)
    name = models.CharField(max_length=80)
    measurement_unit = models.ForeignKey(
        "product.MeasurementUnit",
        related_name="purchase_order_item",
        on_delete=models.DO_NOTHING)
    measurement_quantity = models.PositiveIntegerField(
        validators=[MinValueValidator(1)],
        help_text=_(
            "Measument quantity of a single item in product (Weight/Volume)"))
    quantity = models.PositiveIntegerField(
        validators=[MinValueValidator(1)],
        help_text=_("Quantity of product ordered"))
    unit_amount = MoneyField(
        max_digits=8,
        decimal_places=2,
        help_text=_("Unit purchase amount for product"))
    amount = MoneyField(
        max_digits=8,
        decimal_places=2,
        help_text=_("Total purchase amount for product"))

    def __str__(self):
        return "{} -> {} ({})".format(self.purchase_order.supplier.name,
                                      self.name,
                                      self.purchase_order.fulfillment_date)
