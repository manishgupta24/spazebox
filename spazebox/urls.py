"""spazebox URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from django.views.generic import RedirectView

from spazebox.doc_generator import schema_view

urlpatterns = [
    path('admin/', admin.site.urls),
    path('docs/swagger/', schema_view.with_ui('swagger'), name='docs-swagger'),
    path('docs/redoc/', schema_view.with_ui('redoc'), name='docs-redoc'),
    path("rest-auth/", include("rest_auth.urls")),
    path("rest-auth/registration/", include("rest_auth.registration.urls")),
    path("accounts/", include("allauth.urls"), name="socialaccount_signup"),
    path("users/", include("user.urls"), name="users"),
    path("products/", include("product.urls"), name="products"),
]

# Redirect Urls
urlpatterns += [
    path(
        "rest-auth/registration/account-confirm-email/<key>/",
        RedirectView.as_view(
            url=settings.FRONTEND_DOMAIN +
            "/registration/account-confirm-email?key=%(key)s"),
        name="account_confirm_email"),
    path(
        "password-reset/confirm/<uidb64>/<token>/",
        RedirectView.as_view(
            url=settings.FRONTEND_DOMAIN +
            "/password/reset?uidb64=%(uidb64)s&token=%(token)s"),
        name="password_reset_confirm"),
]
