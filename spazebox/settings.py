"""
Django settings for spazebox project.

Generated by 'django-admin startproject' using Django 2.1.5.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.1/ref/settings/
""" 'phonenumber_field',

import os
import sys

TESTING = sys.argv[1:2] == ['test']

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.1/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '%o=fv7amnd9za-+5&#4vl5((y+33r9lee)p=+nwi#=e$fu=6i1'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = os.environ.get('DEBUG', '1') == '1'

ALLOWED_HOSTS = ['*']

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'django_extensions',  # Django Extended Commands and utilities
    'drf_yasg',  # API Docs - DRF-YASG
    'phonenumber_field',  # PhoneNumber Field
    'rest_framework',  # Django Rest Framework
    'rest_framework.authtoken',  # DRF - Token Authentication
    'rest_auth',  # Rest Auth- For User Login
    'rest_auth.registration',  # Rest Auth- For User Registration
    'allauth',  # AllAuth Package for Auth Management
    'allauth.account',  # AllAuth Package for Account Management
    'allauth.socialaccount',  # AllAuth Package for Account Management
    # User Defined apps
    'address',
    'user',
    'product',
    'purchase',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django_currentuser.middleware.ThreadLocalUserMiddleware',  # Detects User in Request Scope
]

ROOT_URLCONF = 'spazebox.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'spazebox.wsgi.application'

# Database
# https://docs.djangoproject.com/en/2.1/ref/settings/#databases

POSTGRES_DB = os.environ.get('POSTGRES_DB', 'spazebox')
POSTGRES_HOST = os.environ.get('POSTGRES_HOST', 'localhost')
POSTGRES_PORT = os.environ.get('POSTGRES_PORT', '5432')
POSTGRES_USER = os.environ.get('POSTGRES_USER', 'postgres')
POSTGRES_PASSWORD = os.environ.get('POSTGRES_PASSWORD', 'postgres')

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': POSTGRES_DB,
        'USER': POSTGRES_USER,
        'PASSWORD': POSTGRES_PASSWORD,
        'HOST': POSTGRES_HOST,
        'PORT': POSTGRES_PORT,
        'ATOMIC_REQUESTS': os.environ.get('ATOMIC_REQUESTS', '1') == '1'
    }
}

# Password validation
# https://docs.djangoproject.com/en/2.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME':
        'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME':
        'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME':
        'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME':
        'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization
# https://docs.djangoproject.com/en/2.1/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

SITE_ID = 1

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.1/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

##############################################################
#####                 Project Settings                   #####
##############################################################

FRONTEND_DOMAIN = os.environ.get("FRONTEND_DOMAIN", "http://localhost:8000")

# Default MoneyField Currency
DEFAULT_CURRENCY = 'INR'
CURRENCIES = ('INR', )

SMS_ENABLED = True
if TESTING:
    SMS_ENABLED = False

##############################################################
#####                   Auth Settings                    #####
##############################################################

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'allauth.account.auth_backends.AuthenticationBackend',
)

##############################################################
#####                   Rest Settings                    #####
##############################################################

TOKEN_AUTHENTICATION = 'rest_framework.authentication.TokenAuthentication'
REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (TOKEN_AUTHENTICATION, ),
}

##############################################################
####                  Swagger Settings                    ####
##############################################################

SWAGGER_SETTINGS = {
    'USE_SESSION_AUTH': False,
    'SECURITY_DEFINITIONS': {
        'Bearer': {
            'type': 'apiKey',
            'name': 'Authorization',
            'in': 'header'
        }
    },
    'SECURITY_REQUIREMENTS': [{
        "Bearer": []
    }],
    'DOC_EXPANSION': 'list',
    'APIS_SORTER': 'alpha',
    'SHOW_REQUEST_HEADERS': True
}

##############################################################
####                 Rest Auth Settings                   ####
##############################################################

OLD_PASSWORD_FIELD_ENABLED = True
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_EMAIL_SUBJECT_PREFIX = " SpazeBox : "
ACCOUNT_AUTHENTICATION_METHOD = "email"
ACCOUNT_EMAIL_VERIFICATION = "mandatory"
ACCOUNT_EMAIL_CONFIRMATION_HMAC = True
ACCOUNT_USER_DISPLAY = lambda user: user.first_name
REST_AUTH_SERIALIZERS = {
    "USER_DETAILS_SERIALIZER": "user.serializers.UserSerializer",
    'PASSWORD_RESET_SERIALIZER': "core.serializers.PasswordResetSerializer"
}
REST_AUTH_REGISTER_SERIALIZERS = {
    'REGISTER_SERIALIZER': 'core.serializers.RegisterSerializer'
}

if TESTING:
    ACCOUNT_EMAIL_VERIFICATION = None

##############################################################
####                   Email Settings                     ####
##############################################################

CELERY_EMAIL_BACKEND = 'django_smtp_ssl.SSLEmailBackend'  # Email Backend for SES & celery-email
EMAIL_BACKEND = 'djcelery_email.backends.CeleryEmailBackend'  # Django Email Backend on Celery
EMAIL_HOST = 'smtp.gmail.net'
EMAIL_HOST_USER = os.environ.get('GMAIL_USERNAME', 'user')
EMAIL_HOST_PASSWORD = os.environ.get('GMAIL_PASSWORD', 'password')
EMAIL_PORT = 465
EMAIL_USE_SSL = True

DEFAULT_FROM_EMAIL = 'SpazeBox <no-reply@spazebox.com>'
EMAIL_FROM_ADDRESS = DEFAULT_FROM_EMAIL

CELERY_EMAIL_TASK_CONFIG = {
    'queue': 'transactional',
    'rate_limit': '50/m',
}

##############################################################
####                  Celery Settings                     ####
##############################################################

QUEUE_NAME_PREFIX = os.environ.get('ENVIRONMENT', 'dev') + "-"
DEFAULT_BROKER = 'redis://localhost:6379/1'
CELERY_BROKER_URL = os.environ.get('CELERY_BROKER_URL', DEFAULT_BROKER)
CELERY_RESULT_BACKEND = CELERY_BROKER_URL

if TESTING:
    CELERY_RESULT_BACKEND = None
    CELERY_TASK_ALWAYS_EAGER = True

CELERY_TASK_IGNORE_RESULT = False
CELERY_BROKER_TRANSPORT_OPTIONS = {"queue_name_prefix": QUEUE_NAME_PREFIX}

##############################################################
####                  Django Storages                     ####
##############################################################

DATA_UPLOAD_MAX_MEMORY_SIZE = 5242880  # Max File Size in KB
DEFAULT_FILE_STORAGE = "storages.backends.s3boto3.S3Boto3Storage"
AWS_ACCESS_KEY_ID = os.environ.get("S3_ACCESS_KEY", "ACCESS KEY")
AWS_SECRET_ACCESS_KEY = os.environ.get("S3_SECRET_KEY", "SECRET KEY")
AWS_STORAGE_BUCKET_NAME = os.environ.get("S3_BUCKET_NAME", "BUCKET NAME")
AWS_S3_REGION_NAME = os.environ.get("S3_REGION_NAME", "ap-south-1")
AWS_S3_SIGNATURE_VERSION = "s3v4"
AWS_DEFAULT_ACL = None
