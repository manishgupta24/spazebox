from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from rest_framework import permissions

schema_view = get_schema_view(
    openapi.Info(
        title="SpazeBox API Docs",
        default_version='v1.0',
        description="Developer API",
        terms_of_service="https://www.spazebox.com/policies/terms/",
        contact=openapi.Contact(email="manish@spazebox.com")),
    public=True,
    permission_classes=(permissions.AllowAny, ),
)
