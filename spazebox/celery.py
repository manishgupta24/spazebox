import os

from celery import Celery
from celery.schedules import crontab
from django.conf import settings
from kombu import Queue

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'spazebox.settings')

app = Celery('spazebox')

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')

# Define Queues

app.conf.task_queues = (
    Queue('celery'),  # Default Queue
    Queue('transactional'),  # Defined in settings.CELERY_EMAIL_TASK_CONFIG
)

# Define Routes

# app.conf.update(
#     task_routes={
#         'some_task': {
#             'queue': 'test_queue'
#         },
#     }, )

# Add Scheduler

# NOTE: The task defined in beat schedule should NOT use TransactionAwareTask as base class

# app.conf.beat_schedule = {
#     'milestone_due_remiders': {
#         'task': 'notifications.tasks.send_milestone_due_reminders',
#         'schedule': crontab(minute=0, hour=0),  # Everyday at 12 in Night
#     },
# }

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()
