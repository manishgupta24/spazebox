from celery.app.task import Task
from django.core.paginator import Paginator
from django.db import transaction


def chunked_iterator(queryset, chunk_size=25):
    """
    Instead of loading the complete queryset in memory,
    loads the paginated part of queryset.
    """
    paginator = Paginator(queryset, chunk_size)
    for page in range(1, paginator.num_pages + 1):
        for obj in paginator.page(page).object_list:
            yield obj


class TransactionAwareTask(Task):
    """
    Task class which is aware of django db transactions and only executes tasks
    after transaction has been committed
    """
    abstract = True

    def apply_async(self, *args, **kwargs):
        """
        Unlike the default task in celery, this task does not return an async
        result
        """
        transaction.on_commit(lambda: super(TransactionAwareTask, self).apply_async(*args, **kwargs))
