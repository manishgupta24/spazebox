from django.db.models import CharField
from django.core.validators import RegexValidator, MinLengthValidator


class INPanNumberField(CharField):
    """
    A model field that stores the Permanent Account Number issued by Indian Tax authorities.
    """
    description = "PAN Number (ABCDE1234F)"

    def __init__(self, *args, **kwargs):
        kwargs['validators'] = [
            MinLengthValidator(10),
            RegexValidator(regex=r'^[A-Z]{5}[0-9]{4}[A-Z]$')
        ]
        kwargs['max_length'] = 10
        super(INPanNumberField, self).__init__(*args, **kwargs)

    def deconstruct(self):
        name, path, args, kwargs = super(INPanNumberField, self).deconstruct()
        del kwargs['max_length']
        return name, path, args, kwargs


class INGstNumberField(CharField):
    """
    A model field that stores the GST ID Number issued by Indian Tax authorities.
    """
    description = "GST Number (99ABCDE1234F9Z9)"

    def __init__(self, *args, **kwargs):
        kwargs['validators'] = [
            MinLengthValidator(15),
            RegexValidator(
                regex=
                r'^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$'),
            # TODO validate state code
        ]
        kwargs['max_length'] = 15
        super(INGstNumberField, self).__init__(*args, **kwargs)

    def deconstruct(self):
        name, path, args, kwargs = super(INGstNumberField, self).deconstruct()
        del kwargs['max_length']
        return name, path, args, kwargs