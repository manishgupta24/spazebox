from django.core.exceptions import ValidationError
from rest_auth.registration.serializers import \
    RegisterSerializer as DefaultRegisterSerializer
from rest_auth.serializers import \
    PasswordResetSerializer as DefaultPasswordResetSerializer
from rest_framework import serializers


class PasswordResetSerializer(DefaultPasswordResetSerializer):
    """
    Custom version of rest-auth's PasswordResetSerializer that 
    checks email validity and also sets the template for email
    """

    def get_email_options(self):
        """Overridden to support html template"""
        return {
            'html_email_template_name':
            'registration/password_reset_email_html.html'
        }


class RegisterSerializer(DefaultRegisterSerializer):
    """
    Custom version of rest-auth's RegisterSerializer that 
    receives firstname and lastname from the user during registration
    """

    first_name = serializers.CharField()
    last_name = serializers.CharField()

    def get_cleaned_data(self):
        context = super(RegisterSerializer, self).get_cleaned_data()
        context.update({
            "first_name": self.validated_data["first_name"],
            "last_name": self.validated_data["last_name"]
        })
        return context


class BooleanResponseSerializer(serializers.Serializer):
    success = serializers.BooleanField()