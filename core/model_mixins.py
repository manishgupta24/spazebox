from django.db import models
from django_currentuser.db.models import CurrentUserField


class UserTimestampLogMixin(models.Model):
    """
    User Log Fields
    i.e. created, created_by, modified, modified_by
    """
    created = models.DateTimeField(auto_now_add=True)
    created_by = CurrentUserField(related_name="%(class)s_created_by")
    modified = models.DateTimeField(auto_now=True)
    modified_by = CurrentUserField(related_name="%(class)s_modified_by")

    class Meta:
        abstract = True