from django.contrib import admin

from address.models import (Address, City, Country, Locality, State,
                            SupplierAddress)

admin.site.register(
    Country,
    list_display=('id', 'name', 'phone_code'),
    search_fields=('id', 'name', 'phone_code'))

admin.site.register(
    State,
    list_display=('id', 'name'),
    search_fields=('id', 'name'),
    raw_id_fields=('country', ))

admin.site.register(
    City,
    list_display=('id', 'name'),
    search_fields=('id', 'name'),
    raw_id_fields=('state', ))

admin.site.register(
    Locality,
    list_display=('id', 'name', 'pincode'),
    search_fields=('id', 'name', 'pincode'),
    raw_id_fields=('city', ))

admin.site.register(
    Address,
    list_display=('id', 'profile', 'address_line', 'locality'),
    search_fields=('id', 'profile__user__email', 'address_line',
                   'locality__name'),
    raw_id_fields=('locality', 'profile', 'created_by', 'modified_by'))

admin.site.register(
    SupplierAddress,
    list_display=('id', 'supplier', 'address_line', 'locality'),
    search_fields=('id', 'supplier__name', 'address_line', 'locality__name'),
    raw_id_fields=('locality', 'supplier', 'created_by', 'modified_by'))
