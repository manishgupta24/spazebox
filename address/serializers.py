from rest_framework import serializers

from address.models import (Address, City, Country, Locality, State,
                            SupplierAddress)


class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        exclude = []


class StateSerializer(serializers.ModelSerializer):
    class Meta:
        model = State
        exclude = []


class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        exclude = []


class LocalitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Locality
        exclude = []


class ExplodedReadOnlyAddressSerializer(serializers.ModelSerializer):
    city = serializers.IntegerField(source="city.pk")
    city_name = serializers.CharField(source="city.name")
    state = serializers.IntegerField(source="city.state.pk")
    state_name = serializers.CharField(source="city.state.name")
    country = serializers.IntegerField(source="city.state.country.pk")
    country_name = serializers.CharField(source="city.state.country.name")

    class Meta:
        model = Locality
        exclude = []


class BaseAddressSerializer(serializers.ModelSerializer):
    exploded = ExplodedReadOnlyAddressSerializer(
        source="locality", read_only=True)

    class Meta:
        abstract = True


class AddressSerializer(BaseAddressSerializer):
    class Meta:
        model = Address
        exclude = ["created", "modified", "created_by", "modified_by"]


class SupplierAddressSerializer(BaseAddressSerializer):
    class Meta:
        model = SupplierAddress
        exclude = ["created", "modified", "created_by", "modified_by"]
