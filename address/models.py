from django.db import models
from django.utils.translation import ugettext_lazy as _

from core.model_mixins import UserTimestampLogMixin


class Country(models.Model):
    """
    Country Model.
    To store country data.
    """
    name = models.CharField(max_length=50)
    phone_code = models.CharField(
        max_length=5,
        help_text=_("Field to store the phone dialing code e.g. +91, +1"))

    def __str__(self):
        return self.name


class State(models.Model):
    """
    State Model.
    To store state data.
    """
    country = models.ForeignKey(
        "address.Country", related_name="state", on_delete=models.CASCADE)
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class City(models.Model):
    """
    City Model.
    To store city data.
    """
    state = models.ForeignKey(
        "address.State", related_name="city", on_delete=models.CASCADE)
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Locality(models.Model):
    """
    Locality Model.
    To store locality data.
    """
    city = models.ForeignKey(
        "address.City", related_name="locality", on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    pincode = models.CharField(max_length=6)

    def __str__(self):
        return self.name


class BaseAddress(UserTimestampLogMixin):
    """
    Base Address Model (Abstract Model).
    To Store the address of a user or supplier.
    """

    class Meta:
        abstract = True

    locality = models.ForeignKey(
        "address.Locality", related_name="%(class)s", on_delete=models.CASCADE)
    address_line = models.CharField(max_length=200)
    landmark = models.CharField(max_length=100)
    is_default = models.BooleanField(default=False)

    @property
    def city(self):
        return self.locality.city

    @property
    def state(self):
        return self.city.state

    @property
    def country(self):
        return self.state.country

    def __str__(self):
        return self.state + " -> " + self.city + " -> " + self.locality


class Address(BaseAddress):
    """
    Address Model.
    To store address of user.
    """

    profile = models.ForeignKey(
        "user.Profile", on_delete=models.CASCADE, related_name="address")


class SupplierAddress(BaseAddress):
    """
    Supplier Address Model.
    To store address of supplier.
    """

    supplier = models.ForeignKey(
        "purchase.Supplier",
        on_delete=models.CASCADE,
        related_name="supplieraddress")
