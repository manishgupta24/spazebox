from django.contrib.auth.models import User
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver

from user.models import Profile


@receiver(pre_save, sender=User)
def set_email_as_username(sender, instance: User, raw: bool, **kwargs):
    if raw:
        return

    if not instance.pk and not instance.email == instance.username:
        instance.username = instance.email


@receiver(post_save, sender=User)
def create_profile(sender, instance: User, raw: bool, created: bool, **kwargs):
    if raw:
        return

    if created:
        Profile.objects.create(user=instance)