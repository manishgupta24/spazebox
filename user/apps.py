from django.apps import AppConfig


class UserConfig(AppConfig):
    name = 'user'

    def ready(self, *args, **kwargs):
        super(UserConfig, self).ready(*args, **kwargs)

        from user import signals
