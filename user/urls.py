from user.views import OTPConfirmationView, OTPSendView, ResendVerificationEmail

from django.urls import path

urlpatterns = [
    path("otp/send", OTPSendView.as_view(), name="otp-send"),
    path("otp/confirm", OTPConfirmationView.as_view(), name="otp-confirm"),
    path(
        "email/resend-verification-mail",
        ResendVerificationEmail.as_view(),
        name="resend-verification-mail"),
]

app_name = 'users'
