import hashlib
from time import strftime


def generate_otp_code():
    """
    Generate a 4 digit number string based on timestamp.
    """
    time_stamp = strftime('%Y%m%d%H%M%S').encode('utf-8')
    hash_code = hashlib.sha1()
    hash_code.update(time_stamp)
    return int(str(int(hash_code.hexdigest(), 16))[:4])