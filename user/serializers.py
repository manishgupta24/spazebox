from user.models import Phone, Profile

from django.contrib.auth.models import User
from drf_writable_nested import WritableNestedModelSerializer
from phonenumber_field.serializerfields import PhoneNumberField
from rest_framework import serializers

from address.serializers import AddressSerializer


class PhoneSerializer(serializers.ModelSerializer):
    class Meta:
        model = Phone
        fields = ["phone", "verified"]


class ProfileSerializer(WritableNestedModelSerializer):
    address = AddressSerializer(many=True)
    phone = PhoneSerializer(read_only=True)

    class Meta:
        model = Profile
        exclude = ["created", "modified", "created_by", "modified_by"]


class UserSerializer(WritableNestedModelSerializer):
    profile = ProfileSerializer()

    class Meta:
        model = User
        exclude = [
            "groups", "user_permissions", "is_superuser", "password",
            "is_staff", "is_active"
        ]


###############################################################
#####                Custom API Serializers               #####
###############################################################


class OTPSendSerializer(serializers.Serializer):
    phone = PhoneNumberField()
    send_otp = serializers.BooleanField()


class OTPConfirmationSerializer(serializers.Serializer):
    otp = serializers.IntegerField(max_value=9999, min_value=1000)
