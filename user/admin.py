from user.models import Phone, Profile

from django.contrib import admin

admin.site.register(
    Profile,
    list_display=('id', 'user'),
    search_fields=('id', 'user__email', 'user__first_name'),
    raw_id_fields=('user', 'phone', 'created_by', 'modified_by'))

admin.site.register(
    Phone,
    list_display=('id', 'phone', 'verified'),
    search_fields=('id', 'phone', 'verified'),
    raw_id_fields=('created_by', 'modified_by'))
