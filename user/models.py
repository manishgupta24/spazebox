import phonenumbers
from django.conf import settings
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from phonenumber_field.modelfields import PhoneNumberField

from core.model_mixins import UserTimestampLogMixin
from user.utils import generate_otp_code


class Profile(UserTimestampLogMixin):
    """
    Profile Model.
    To store extra data of user like phone or address.
    """
    user = models.OneToOneField(
        "auth.User", related_name="profile", on_delete=models.CASCADE)
    phone = models.OneToOneField(
        "user.Phone",
        related_name="profile",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        default=None)

    def __str__(self):
        return self.user.email


class Phone(UserTimestampLogMixin):
    verified = models.BooleanField(default=False)
    code = models.IntegerField(
        default=generate_otp_code,
        validators=[MaxValueValidator(9999),
                    MinValueValidator(1000)])
    sent = models.BooleanField(default=False)
    phone = PhoneNumberField(null=False, blank=False, db_index=True)

    def __str__(self):
        return "{} -> {}".format(self.profile, self.phone)

    def send_confirmation(self, phone, send_sms=True):
        if phonenumbers.is_valid_number(self.phone):
            if not self.phone == phone:
                self.phone = phone
                self.save()
            if settings.SMS_ENABLED and send_sms:
                # call celery task to send sms here
                pass
            self.sent = True
            self.verified = False
            self.save()
            return True
        return False

    def confirm(self, code):
        if self.code == code:
            self.verified = True
            self.sent = False
            self.code = generate_otp_code()
            self.save()
        return self.verified
