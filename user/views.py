from user.models import Phone
from user.serializers import (OTPConfirmationSerializer, OTPSendSerializer,
                              PhoneSerializer)

from django.http import Http404
from drf_yasg.utils import swagger_auto_schema
from rest_framework import response, status, views
from rest_framework.permissions import IsAuthenticated

from core.serializers import BooleanResponseSerializer


class OTPSendView(views.APIView):
    permission_classes = (IsAuthenticated, )

    def resend_or_create(self, request, data):
        phone_object, _ = Phone.objects.get_or_create(
            profile=request.user.profile)
        sms_sent = phone_object.send_confirmation(data["phone"],
                                                  data["send_otp"])
        setattr(phone_object, "sent", sms_sent)
        return phone_object

    @swagger_auto_schema(
        request_body=OTPSendSerializer, responses={200: PhoneSerializer})
    def post(self, request, *args, **kwargs):
        serializer = OTPSendSerializer(data=request.data)
        if not serializer.is_valid():
            return response.Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        phone_object = self.resend_or_create(request,
                                             serializer.validated_data)
        return response.Response(
            PhoneSerializer(phone_object).data, status=status.HTTP_200_OK)


class OTPConfirmationView(views.APIView):
    permission_classes = (IsAuthenticated, )

    @swagger_auto_schema(
        request_body=OTPConfirmationSerializer,
        responses={200: BooleanResponseSerializer})
    def post(self, request, *args, **kwargs):
        serializer = OTPConfirmationSerializer(data=request.data)
        if not serializer.is_valid():
            return response.Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        phone_object = getattr(request.user.profile, "phone", None)
        if not phone_object:
            raise Http404({"phone": ["No valid Phone Number Found"]})

        success = phone_object.confirm(serializer.validated_data["otp"])
        return response.Response(
            dict(success=success), status=status.HTTP_200_OK)


class ResendVerificationEmail(views.APIView):
    permission_classes = (IsAuthenticated, )

    @swagger_auto_schema(responses={200: BooleanResponseSerializer})
    def post(self, request, *args, **kwargs):
        email_address = request.user.emailaddress_set.first()
        response_dict = dict(success=False)
        if not email_address.verified:
            email_address.send_confirmation(request=request)
            response_dict = dict(success=True)
        return response.Response(response_dict, status=status.HTTP_200_OK)
